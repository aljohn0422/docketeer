const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
        args: [
            '--no-sandbox',
        ]
    });
    const v = await browser.version();
    console.log(v);
    const page = await browser.newPage();
    await page.goto('https://google.com');
    const title = await page.title();
    console.log(title);

    await browser.close();
})();